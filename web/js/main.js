/*global $, window, this, console, fullpage_api*/
/*jslint this:true*/


$(function () {

    'use strict';

    var winWidth = $(window).outerWidth();

    $(window).on('load resize', function () {
        $('.pagination').addClass('color_type');
        $('.img').height($(window).height());
    });

    $('.product-tab li').on('click', function (e) {
        e.preventDefault();
        var idx = $(this).index();

        $('.product-tab li').removeClass('on');
        $(this).addClass('on');

        $('.product-tabBox>div').removeClass('on');
        $('.product-tabBox>div').eq(idx).addClass('on');
    });

    var downHeaderColorRemove = function () {
        if (winWidth > 1100) {
            $('.top').fadeOut(500);
            $('.top').removeClass('color_type');
            $('.top').fadeIn(500);
        } else {
            $('.mobiletop').fadeOut(500);
            $('.mobiletop').removeClass('color_type');
            $('.mobiletop').fadeIn(500);
        }
    };

    var downHeaderColorAdd = function () {
        if (winWidth > 1100) {
            $('.top').fadeOut(500);
            $('.top').addClass('color_type');
            $('.top').fadeIn(500);
        } else {
            $('.mobiletop').fadeOut(500);
            $('.mobiletop').addClass('color_type');
            $('.mobiletop').fadeIn(500);
        }
    };

    var upHeaderColorRemove = function () {
        if (winWidth > 1100) {
            $('.top').css('opacity', '0');
            $('.top').removeClass('color_type');
            setTimeout(function () {
                $('.top').animate({
                    'opacity': '1'
                }, 950);
            }, 500);
        } else {
            $('.mobiletop').css('opacity', '0');
            $('.mobiletop').removeClass('color_type');
            setTimeout(function () {
                $('.mobiletop').animate({
                    'opacity': '1'
                }, 950);
            }, 500);
        }
    };

    var upHeaderColorAdd = function () {
        if (winWidth > 1100) {
            $('.top').css('opacity', '0');
            $('.top').addClass('color_type');
            setTimeout(function () {
                $('.top').animate({
                    'opacity': '1'
                }, 950);
            }, 500);
        } else {
            $('.mobiletop').css('opacity', '0');
            $('.mobiletop').addClass('color_type');
            setTimeout(function () {
                $('.mobiletop').animate({
                    'opacity': '1'
                }, 950);
            }, 500);
        }
    };

    $('#fullpage').fullpage({
        licenseKey: 'EA28FBC0-E0D841BE-BA4612E3-869492EA',
        verticalCentered: true,
        controlArrows: false,
        normalScrollElements: '.agreement-bg, .privacy-bg',
        afterRender: function () {
            $('.pagination').on('click', function (e) {
                e.preventDefault();
                fullpage_api.moveSectionDown();
            });
        },
        onLeave: function (index, nextIndex, direction) {
            var slide = $(this);
            var idx = '';
            if (direction === 'down') {
                
                // Scroll Down
                if (slide[0].index === 0) {
                    $('.pagination').removeClass('color_type');
                    downHeaderColorAdd();
                } else if (slide[0].index === 1) {
                    downHeaderColorRemove();
                    $('.pagination').addClass('color_type');
                } else if (slide[0].index === 2) {
                    downHeaderColorAdd();
                    $('.pagination').removeClass('color_type');
                }else if(slide[0].index === 3){
                    if (winWidth > 1110) {
                        $(".top").fadeOut(500);
                        $('.top').addClass('color_type');
                    } else {
                        $('.mobiletop').addClass('color_type');
                        $('.mobiletop').fadeOut(500);
                    }
                } else {
                    downHeaderColorRemove();
                }
            } else if (direction === 'up') {
               

                // Scroll Up
                 if (slide[0].index === 4) {
                    upHeaderColorAdd();
                   
                    if (winWidth > 1110) {
                        $('.top').fadeIn(500);
                    } else {
                        $('.mobiletop').fadeIn(500);
                    }
                } else if (slide[0].index === 3) {
                    upHeaderColorRemove();
                    $('.pagination').addClass('color_type');
                    $('.top, .mobiletop').css('background-color', 'transparent');
                } else if (slide[0].index === 2) {
                    upHeaderColorAdd();
                    $('.pagination').removeClass('color_type');
                } else if (slide[0].index === 1) {
                    $('.pagination').addClass('color_type');
                    upHeaderColorRemove();
                } else {
                    upHeaderColorRemove();
                }
            }
        }
    });

    var t1, t2, t3, t4, t5, t6, t7;
    // overlay
    var overlayFunc = function () {
        t1 = setTimeout(function () {
            $('.slide-text').removeClass('is_show');
            $('.page_overlay').addClass('is_show');
            t2 = setTimeout(function () {
                $('.slick-current').addClass('is_active');
                $('.page_overlay').addClass('is_hide');
                $('.page_dimmed').addClass('is_show');
                t4 = setTimeout(function () {
                    $('.page_overlay').removeClass('is_show');
                    $('.page_overlay').removeClass('is_hide');
                    t5 = setTimeout(function () {
                        $('.page_dimmed').addClass('is_hide');
                    }, 500);
                    t6 = setTimeout(function () {
                        $('.page_dimmed').removeClass('is_show');
                        $('.page_dimmed').removeClass('is_hide');
                    }, 900);
                }, 700);
                t3 = setTimeout(function () {
                    $('.slick-current .slide-text').addClass('is_show');
                    $('.page_dimmed').addClass('is_hide');
                    t7 = setTimeout(function () {
                        $('.page_dimmed').removeClass('is_hide');
                    }, 1900);
                });
            }, 700);
        });
    };

    var currentIdx;
    // slider (first, init)
    $('.slick-slider').on('init', function (e, slick, currentSlide) {
        overlayFunc();
        $('.slick-current').removeClass('is_active').addClass('is_reset');
        setTimeout(function () {
            $('.slick-current').removeClass('is_reset').addClass('is_active');
        });
    });


    $('.slick-slider').slick({
        autoplay: true,
        infinite: false,
        arrows: false,
        autoplaySpeed: 5000,
        pauseOnHover: false
    });

    $('.slick-slider').on('beforeChange', function () {
        clearTimeout(t1);
        clearTimeout(t2);
        clearTimeout(t3);
        clearTimeout(t4);
        clearTimeout(t5);
        clearTimeout(t6);
        clearTimeout(t7);
        overlayFunc();
    });

    $('.slick-slider').on('afterChange', function (e, slick, currentSlide) {
        currentIdx = (currentSlide ? currentSlide : 0) + 1;
        $('.slick-slide').eq(currentIdx - 1).siblings().removeClass('is_active');
        if (currentIdx === slick.slideCount) {
            $('.slick-slider').slick('slickSetOption', 'autoplay', false, false);
        }
    });
});