/*global $, window, console, location*/
/*jslint this:true*/

$(function () {
    'use strict';

    $('.menu-btn').on('click', function () {
        $('.menu-bg, .mobile-menu').show();
    });

    var root = $(location).attr('pathname').split('/')[2];

    var idx = null;

    switch (root) {
    case "index":
        idx = 0;
        break;
    case "products":
        idx = 1;
        break;
    case "about":
        idx = 2;
        break;
    case "corporate":
        idx = 3;
        break;
  
    }

    $('.gnb li').removeClass('on');
    $('.gnb li').eq(idx).addClass('on');

    $('.close-btn').on('click', function () {
        $('.menu-bg').hide();
    });

    
    // 푸터 및 약관페이지 pop 처리

    var $footerNavList = $('.footer-nav li');
    var $mobilerNavList = $('.mobile-nav li');
    var $agreement = $('.agreement-bg');
    var $privacy = $('.privacy-bg');

    $footerNavList.eq(0).on('click', function (e) {
        e.preventDefault();
        $agreement.show();
    });

    $footerNavList.eq(1).on('click', function (e) {
        e.preventDefault();
        $privacy.show();
    });

    $mobilerNavList.eq(0).on('click', function (e) {
        e.preventDefault();
        $agreement.show();
    });

    $mobilerNavList.eq(1).on('click', function (e) {
        e.preventDefault();
        $privacy.show();
    });

    $('.agreement-close').on('click', function () {
        $privacy.hide();
        $agreement.hide();
    });
});