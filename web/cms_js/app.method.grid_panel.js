/*global require, module, console, app, $, Ext, _, window*/
/*jslint this:true*/
(function () {
    'use strict';

    var proxyMaker = function (obj, url) {
        var proxyBody = {};
        if (obj.use_paging) {
            proxyBody = {
                type: 'ajax',
                timeout: 120000,
                api: {
                    read: url
                },
                reader: {
                    root: 'data',
                    totalProperty: 'count'
                }
            };
        } else {
            proxyBody = {
                pageParam: undefined,
                startParam: undefined,
                limitParam: undefined,
                type: 'ajax',
                timeout: 120000,
                api: {
                    read: url
                },
                reader: {
                    root: 'data'
                }
            };
        }
        return proxyBody;
    };

    var winStore = function (obj) {
        var stringTypes = _.difference(obj.fields, obj.numTypes);
        var url = obj.url;
        var proxyBody = proxyMaker(obj, url);

        var storeBody = {
            fields: obj.fields,
            autoLoad: true,
            model: app.common.common_model(stringTypes, obj.numTypes),
            sortOnLoad: true,
            proxy: proxyBody
        };

        if (obj.use_filter) {
            // storeBody.filterOnLoad = true;
            storeBody.listeners = {
                load: function (store, records) {
                    // debugger;
                    var removed = [];
                    _.each(records, function (el) {
                        if (el.raw.status === '환불' || el.raw.status === '업체관리-기타') {
                            removed.push(store.getAt(el.index));
                        }
                    });
                    // debugger;
                    store.remove(removed);
                }
            };
        }
        if (obj.use_paging) {
            storeBody.pageSize = app.userData.pageSize;
        }
        var store = Ext.create('Ext.data.Store', storeBody);
        return store;
    };

    var tbarMaker = function (obj) {
        var tbars = [];

        tbars.push(app.buttons.reload);
        tbars.push('-');
        if (obj.use_search) {
            tbars.push(app.buttons.search());
            tbars.push('-');
        }
        if (obj.additional_tbars && obj.additional_tbars.length > 0) {
            tbars = tbars.concat(obj.additional_tbars);
        }
        if (obj.use_excel) {
            tbars.push('-');
            tbars.push(app.buttons.real_excel());
        }
        return tbars;
    };

    var basicMaker = function (obj) {
        var winstore = winStore(obj);
        var dockedItems = [];
        if (obj.use_paging) {
            dockedItems = [{
                xtype: 'pagingtoolbar',
                dock: 'bottom',
                store: winstore,
                displayInfo: true,
                displayMsg: '{0} - {1}',
                emptyMsg: ''
            }];
        }
        var baseObj = {
            multiSelect: false,
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2
                }),
                'bufferedrenderer'
            ],
            dockedItems: dockedItems,
            store: winstore,
            columns: obj.cols,
            viewConfig: {
                bufferedRenderer: false,
                preserveScrollOnRefresh: true,
                emptyText: '데이터가 없습니다.'
            },
            features: [{
                ftype: 'summary',
                dock: 'top'
            }],
            listeners: {},
            tbar: tbarMaker(obj)
        };
        if (obj.features) {
            baseObj.features = obj.features;
        }
        return baseObj;
    };
    var winPanel = function (obj, onlyPanel) {
        var baseObj = basicMaker(obj);
        if (onlyPanel && obj.title) {
            baseObj.title = obj.title;
        }
        if (obj.uid) {
            baseObj.id = obj.uid;
        }
        if (obj.use_checkbox) {
            baseObj.selModel = Ext.create('Ext.selection.CheckboxModel');
        }
        if (obj.features) {
            baseObj.features = obj.features;
        }
        if (obj.plugins) {
            _.each(obj.plugins, function (pp) {
                baseObj.plugins.push(pp);
            });
        }
        if (obj.collapsed) {
            baseObj.collapsed = obj.collapsed;
        }
        if (obj.listeners) {
            _.extend(baseObj.listeners, obj.listeners);
        }
        if (obj.viewConfig) {
            _.extend(baseObj.viewConfig, obj.viewConfig);
        }
        if (baseObj.columns) {
            return Ext.create('Ext.grid.Panel', baseObj);
        } else {
            return false;
        }
    };

    var grid_panel = function (obj, onlyPanel) {
        if (Ext.getCmp(obj.uid)) {
            return false;
        }
        if (onlyPanel) {
            return winPanel(obj, onlyPanel);
        }
        var chartName = obj.title;
        var $win = $(window);
        var temp = {
            title: chartName,
            width: obj.width || $win.width() * 0.6,
            height: obj.height || $win.height() * 0.6,
            items: [winPanel(obj)],
            border: false,
            id: obj.uid
        };
        if (obj.opened && obj.opened === true) {
            temp.opened = obj.opened;
        } else {
            temp.opened = false;
        }
        if (obj.maximized !== undefined) {
            temp.maximized = obj.maximized;
        }
        console.log(temp);
        app.method.common_panel(temp, false);
    };
    app.method.grid_panel = grid_panel;
}());