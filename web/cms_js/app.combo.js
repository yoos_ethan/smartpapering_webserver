/*global require, module, console, app, async, $, _, Ext*/
(function () {
    'use strict';

    app.combo = {};
    app.combo.employees = [];
    app.combo.employee_map = {};
    app.combo.experts = [];
    app.combo.expert_map = {};
    app.combo.partners = [];
    app.combo.partner_map = {};
    app.combo.securities = [];
    app.combo.security_map = {};
    app.combo.products = [];
    app.combo.product_map = {};

    app.combo.remote = [{
        name: '신규가입',
        value: '신규가입'
    }, {
        name: '계좌개설',
        value: '계좌개설'
    }, {
        name: '원격진행',
        value: '원격진행'
    }, {
        name: '연동완료',
        value: '연동완료'
    }];
    app.combo.col06 = [{
        name: '-',
        value: '-'
    }, {
        name: '정기결제',
        value: '정기결제'
    }, {
        name: '일괄결제',
        value: '일괄결제'
    }];
    app.combo.col09 = [{
        name: 'NO',
        value: 'NO'
    }, {
        name: 'YES',
        value: 'YES'
    }, {
        name: '진행중',
        value: '진행중'
    }];

    app.combo.col15 = [{
        name: 'NO',
        value: 'NO'
    }, {
        name: 'YES',
        value: 'YES'
    }, {
        name: '진행중',
        value: '진행중'
    }];

    app.combo.col16 = [{
        name: 'NO',
        value: 'NO'
    }, {
        name: 'YES',
        value: 'YES'
    }, {
        name: '진행중',
        value: '진행중'
    }];

    app.combo.col17 = [{
        name: 'NO',
        value: 'NO'
    }, {
        name: 'YES',
        value: 'YES'
    }, {
        name: '진행중',
        value: '진행중'
    }];
    app.combo.col18 = [{
        name: 'NO',
        value: 'NO'
    }, {
        name: 'YES',
        value: 'YES'
    }, {
        name: '진행중',
        value: '진행중'
    }];
    app.combo.col19 = [{
        name: 'NO',
        value: 'NO'
    }, {
        name: 'YES',
        value: 'YES'
    }, {
        name: '진행중',
        value: '진행중'
    }];
    app.combo.col24 = [{
        name: 'NO',
        value: 'NO'
    }, {
        name: 'YES',
        value: 'YES'
    }, {
        name: '진행중',
        value: '진행중'
    }];
    app.combo.col43 = [{
        name: 'NO',
        value: 'NO'
    }, {
        name: 'YES',
        value: 'YES'
    }, {
        name: '진행중',
        value: '진행중'
    }];
    app.combo.col25 = [{
        name: '개인',
        value: '개인'
    }, {
        name: '내부(본사)',
        value: '내부(본사)'
    }, {
        name: '일괄(법인)',
        value: '일괄(법인)'
    }, {
        name: '정기(회원)',
        value: '정기(회원)'
    }];

    app.combo.status = [{
        name: '-',
        value: '-'
    }, {
        name: '미입금',
        value: '미입금'
    }, {
        name: '연동중지',
        value: '연동중지'
    }, {
        name: '환불',
        value: '환불'
    }, {
        name: '업체관리-기타',
        value: '업체관리-기타'
    }, {
        name: '연동비율 조정필요',
        value: '연동비율 조정필요'
    }];

    app.combo.col39 = [{
        name: '-',
        value: '-'
    }, {
        name: '평생',
        value: '평생'
    }];
    app.combo.col40 = [{
        name: '5',
        value: '5'
    }, {
        name: '10',
        value: '10'
    }];
    app.combo.col34 = [{
        name: 'NO',
        value: 'NO'
    }, {
        name: 'YES',
        value: 'YES'
    }];
    _.each(_.range(1, 13), function (el) {
        app.combo.col39.push({
            name: el + '개월',
            value: el + '개월'
        });
    });

    var fetchCombo = function (url, opt, done) {
        $.ajax({
            method: 'GET',
            url: url,
            async: false,
            success: function (data) {
                app.combo[opt.comboKey] = [];
                app.combo[opt.mapKey] = {};
                _.each(data, function (dd, idx) {
                    var name = dd[opt.nameKey];
                    var value = dd[opt.valueKey];
                    app.combo[opt.comboKey][idx] = {
                        name: name,
                        value: value
                    };
                    app.combo[opt.mapKey][value] = dd;
                });
                if (done) {
                    return done(null, data);
                }
            },
            error: function (xhr) {
                console.error('fetch ' + url, xhr.status);
                if (done) {
                    return done();
                }
            }
        });
    };

    var postPush = function(title, body, userToken){
            
            $.ajax({
                type: 'POST',
                url: '/api/push/ordermessages',
                data: {
                    to : userToken,
                    title : title,
                    body : body
                },
                error: function (err) {
                    console.log(err);
                }
            })
        
    }

    var numberWithCommas = function(num){
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    app.combo.sendOrderPush = function (orderData, status) {
            
            $.ajax({
                type : 'GET',
                url : '/api/user/special?unique='+orderData.order_id,
                success : function(res){
                    var userData = res.data;
                    var pushToken = userData.push_token
                    var title = status;
                    var body;
                    
                    if(userData.islogin && userData.use_push){
                        switch(status){
                            case '시공진행' :
                                body = orderData.customer_name+"님의 시공이 진행 중 입니다."
                                postPush(title, body, pushToken);
                                break;
                            case '시공완료' :
                                body = orderData.customer_name+"님의 시공이 완료 되었습니다."
                                postPush(title, body, pushToken);
                                break;
                            case '거래취소' :
                                body = orderData.customer_name+"님의 시공이 취소 되었습니다."
                                postPush(title, body, pushToken);
                                break;
                            default :
                                break;
                        }
                    }
                    
                },
                error : function(err){
                    console.log(err)
                }
            })
    }

    app.combo.sendEndOrderPush = function(orderData, point){        
        var commaPoint = numberWithCommas(point);

            $.ajax({
                type : 'GET',
                url : '/api/user/special?unique='+orderData.order_id,
                success : function(res){
                    var userData = res.data;
                    var pushToken = userData.push_token
                    var title = '포인트 적립';
                    var body = orderData.customer_name+'님의 시공이 완료되어 '+commaPoint+"원이 적립 되었습니다.";
                    if(userData.islogin && userData.use_push){
                        postPush(title, body, pushToken);
                    }   
                },
                error : function(err){
                    console.log(err)
                }
            })
    }

    app.combo.sendWithdrawPush = function(withdrawData, status){
        var withdrawPrice = numberWithCommas(withdrawData.withdraw_price);
        if(status === '완료'){
            $.ajax({
                type : 'GET',
                url : '/api/user/special?unique='+withdrawData.user_id,
                success : function(res){
                    var userData = res.data;
                    var pushToken = userData.push_token
                    var title = '출금완료';
                    var body = "신청하신 "+withdrawPrice+'원의 출금이 완료되었습니다.';
                    if(userData.islogin && userData.use_push){
                        postPush(title, body, pushToken);
                    }   
                },
                error : function(err){
                    console.log(err)
                }
            })
        }
    }

    var postOrderTele = function(txt){
        $.ajax({
            type: 'POST',
            url: '/api/push/ordertele',
            data: {
                text : txt
            },
            error: function (err) {
                console.log(err);
            }
        })
    }
    
    app.combo.sendOrderTele = function(orderData, status){
        var teletxt = "***시공 변경사항 실시간 알림***"+'\n'+"고객명: "+
                    orderData.customer_name+'\n'+"고객연락처: "+
                    orderData.customer_telephone+'\n'+'상태: '+status;
        
        postOrderTele(teletxt);
    }

    var postWithdrawTele = function(txt){
        $.ajax({
            type: 'POST',
            url: '/api/push/withdrawtele',
            data: {
                text : txt
            },
            error: function (err) {
                console.log(err);
            }
        })
    }

    app.combo.sendWithdrawTele = function(withdrawData, status){
        var withdrawTele = "***출금 변경사항 실시간 알림***"+'\n'+"리셀러명: "+
                        withdrawData.user_name+'\n'+"은행명: "+withdrawData.bank+'\n'+"계좌번호: "+
                        withdrawData.account_num+'\n'+"예금주: "+withdrawData.account_holder+'\n'+'상태: '+status;

        postWithdrawTele(withdrawTele);               
    }

    app.combo.reloadAll = function () {
        var withdraw_store = Ext.getCmp('withdraw').getStore();
        var endWithdraw_store = Ext.getCmp('end_withdraw').getStore();
        var order_store = Ext.getCmp('order').getStore();
        var endOrder_store = Ext.getCmp('end_order').getStore();
        var cancelOrder_store = Ext.getCmp('cancel_order').getStore();
        var user_store = Ext.getCmp('member').getStore();

        withdraw_store.reload();
        endWithdraw_store.reload();
        order_store.reload();
        endOrder_store.reload();
        cancelOrder_store.reload();
        user_store.reload();
    };

    app.combo.comboTem = function (poptions, extra) {
        var poptionsProto = {
            title: '타이틀',
            default_data: [],
            name: null,
            default_value: null,
            disabled: false,
            hidden: false
        };
        var opt = _.extend(poptionsProto, poptions);
        // var option = app.formComp.getUid();
        var comboStore = Ext.create('Ext.data.Store', {
            fields: ['value', 'name'],
            data: opt.default_data
        });

        var result = {
            emptyText: opt.title + '를(을) 선택하세요',
            xtype: 'combo',
            fieldLabel: opt.title,
            store: comboStore,
            name: opt.name,
            displayField: 'name',
            valueField: 'value',
            queryMode: 'local',
            forceSelection: opt.forceSelection || true,
            hidden: opt.hidden,
            disabled: opt.disabled,
            value: opt.default_value || '',
            editable: false,
            labelSeparator: ''
        };
        if (poptions.allowBlank) {
            result.allowBlank = poptions.allowBlank;
        }
        if (extra) {
            result = _.extend(result, extra);
        }
        return result;
    };

}());