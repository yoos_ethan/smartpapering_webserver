/*global require, module, console, _, app, Ext*/
(function () {
    'use strict';
    var common_store = function (obj) {
        var stringTypes = _.difference(obj.fields, obj.numTypes);
        var url = obj.url;
        var proxyBody = {
            pageParam: undefined,
            startParam: undefined,
            limitParam: undefined,
            type: 'ajax',
            timeout: 120000,
            api: {
                read: url
            },
            reader: {
                root: ''
            }
        };
        var storeBody = {
            fields: obj.fields,
            autoLoad: true,
            model: app.common.common_model(stringTypes, obj.numTypes),
            sortOnLoad: true,
            proxy: proxyBody
        };
        var store = Ext.create('Ext.data.Store', storeBody);
        return store;
    };
    app.common.common_store = common_store;
}());