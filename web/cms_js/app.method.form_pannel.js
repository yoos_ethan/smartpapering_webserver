/*global require, module, console, app, $, Ext, _, window*/
(function(){
    'use strict'
  
    var enroll_form = function(opt, type){
        var enrollOpt = {
            buttonAlign : 'center',
            bodyPadding : 40,
            fullscreen: true,
            layout: 'form',
            id : type.tbody,
            items : opt,
            buttons:[{
                text: '등록',
                width : 100,
                height: 25,
                id : type.btn,
                handler:function(){
                    var form = this.up('form');
                    app.validater(form.getValues());
                    if(form.isValid()){
                        Ext.Ajax.request({
                            url : type.url,
                            method : 'PUT',
                            cors : true,
                            useDefaultXhrHeader:false,
                            params : form.getValues(),
                            success: function() {
                                Ext.Msg.alert('성공', type.name + '등록을 완료하였습니다.');
                            },
                            failure: function() {
                                Ext.Msg.alert('실패', type.name + '등록이 실패하였습니다. 잠시 후 다시 시도해주세요.');
                            }
                        })
                    
                        // form.submit({
                        //     success: function(form, action) {
                        //         //    Ext.Msg.alert('성공', type.name + '등록을 완료하였습니다.');
                        //         Ext.Msg.alert('성공', action.response);
                        //     },
                        //     failure: function(form, action) {
                        //         // Ext.Msg.alert('실패', type.name + '등록이 실패하였습니다. 잠시 후 다시 시도해주세요.');
                        //         Ext.Msg.alert('실패', action.failureType);
                        //     }
                        // });
                    }else{
                        Ext.Msg.alert('등록 실패', "양식에 맞게 다시 작성해주세요.")
                    }
                        
                    
                }
            }]
        }
        var enrollWin = Ext.create('Ext.form.Panel', enrollOpt);

        return enrollWin;
    }

    app.method.form_pannel = enroll_form;

}());