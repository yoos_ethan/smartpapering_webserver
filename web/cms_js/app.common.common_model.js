/*global require, module, console, Ext, app, _*/
(function () {
    'use strict';
    var pushTo = function (arr, type) {
        var result = [];
        _.each(arr, function (dd) {
            result.push({
                name: dd,
                type: type
            });
        });
        return result;
    };
    var common_model = function (stringTypes, numTypes) {
        var fieldsSet = [];
        fieldsSet = fieldsSet.concat(pushTo(stringTypes, 'string'));
        fieldsSet = fieldsSet.concat(pushTo(numTypes, 'number'));
        var model = Ext.define('model', {
            extend: 'Ext.data.Model',
            fields: fieldsSet
        });
        return model;
    };
    app.common.common_model = common_model;
}());