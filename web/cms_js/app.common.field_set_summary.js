/*global require, module, console, app*/
(function () {
    'use strict';
    var fbasic = app.common.field_maker.basic;
    var formatter = app.common.formatter;
    var fcombo = app.common.field_maker.combobox;
    var remote_text_class = {
        '신규가입': 'font-skyblue',
        '계좌개설': 'font-purple',
        '원격진행': 'font-yellow',
        '연동완료': 'font-green'
    };

    var combo_text_class = {
        '업체관리-기타': 'font-orange',
        '연동비율 조정필요': 'font-orange',
        '업체관리': 'font-orange',
        '미입금': 'font-skyblue',
        '연동중지': 'font-purple',
        '환불': 'font-red'
    };

    var field_set_summary = function () {
        return {
            remote: function () {
                return fcombo({
                    header: '리모트',
                    dataIndex: 'remote',
                    align: 'center',
                    width: 90,
                    comboKey: 'remote',
                    renderer: function (val) {
                        var font_class = '';
                        if (remote_text_class[val]) {
                            font_class = remote_text_class[val];
                        }
                        return '<span class="' + font_class + '">' + val + '</span>';
                    }
                });
            },
            status: function () {
                return fcombo({
                    header: '상태',
                    dataIndex: 'status',
                    align: 'left',
                    width: 130,
                    comboKey: 'status',
                    disabled: app.roleSettings.status_disabled(),
                    renderer: function (val) {
                        var font_class = '';
                        if (combo_text_class[val]) {
                            font_class = combo_text_class[val];
                        }
                        return '<span class="' + font_class + '">' + val + '</span>';
                    }
                });
            },
            col01: function () {
                return fbasic({
                    header: '원격담당',
                    dataIndex: 'col01',
                    width: 60,
                    align: 'left',
                    text_editable: true,
                    disabled: app.roleSettings.col01_disabled(),
                    nullable: true
                });
            },
            col02: function (use_disable) {
                var disabled = app.roleSettings.col02_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fbasic({
                    header: '계약일자',
                    dataIndex: 'col02',
                    align: 'left',
                    width: 80,
                    text_editable: true,
                    disabled: disabled,
                    nullable: true
                });
            },
            col03: function (use_disable) {
                var disabled = app.roleSettings.col03_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fcombo({
                    header: '법인명',
                    dataIndex: 'col03',
                    align: 'left',
                    width: 110,
                    comboKey: 'partners',
                    mapKey: 'partner_map',
                    disabled: disabled
                });
            },
            col04: function (use_disable) {
                var disabled = app.roleSettings.col04_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fbasic({
                    header: '회원명',
                    dataIndex: 'col04',
                    align: 'left',
                    width: 80,
                    text_editable: true,
                    disabled: disabled,
                    nullable: true
                });
            },
            col05: function (use_disable) {
                var disabled = app.roleSettings.col05_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fbasic({
                    header: '연락처',
                    dataIndex: 'col05',
                    align: 'left',
                    width: 92,
                    text_editable: true,
                    disabled: disabled,
                    nullable: true
                });
            },
            col06: function () {
                return fcombo({
                    header: 'AWS 비용청구',
                    dataIndex: 'col06',
                    align: 'center',
                    width: 80,
                    comboKey: 'col06',
                    mapKey: 'col06',
                    renderer: formatter.yes_no
                });
            },
            col07: function () {
                return fbasic({
                    header: '계약금액(만원)',
                    dataIndex: 'col07',
                    align: 'right',
                    width: 120,
                    text_editable: true,
                    nullable: true
                });
            },
            col08: function () {
                return fbasic({
                    header: '주소',
                    dataIndex: 'col08',
                    align: 'left',
                    width: 400,
                    text_editable: true,
                    nullable: true
                });
            },
            col09: function () {
                return fcombo({
                    header: '전자계약',
                    dataIndex: 'col09',
                    align: 'center',
                    width: 80,
                    comboKey: 'col09',
                    mapKey: 'col09',
                    renderer: formatter.yes_no
                });
            },
            col11: function () {
                return fbasic({
                    header: '환불일자',
                    dataIndex: 'col11',
                    align: 'left',
                    width: 90,
                    text_editable: true
                });
            },
            col12: function () {
                return fbasic({
                    header: '투자금',
                    dataIndex: 'col12',
                    align: 'right',
                    width: 120,
                    text_editable: true
                });
            },
            col14: function (use_disable) {
                var disabled = app.roleSettings.col14_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fcombo({
                    header: '증권사',
                    dataIndex: 'col14',
                    align: 'left',
                    width: 96,
                    comboKey: 'securities',
                    mapKey: 'security_map',
                    disabled: disabled
                });
            },
            col15: function (use_disable) {
                var disabled = app.roleSettings.col15_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fcombo({
                    header: '계좌개설',
                    dataIndex: 'col15',
                    align: 'center',
                    width: 80,
                    comboKey: 'col15',
                    mapKey: 'col15',
                    disabled: disabled,
                    renderer: formatter.yes_no
                });
            },
            col16: function (use_disable) {
                var disabled = app.roleSettings.col16_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fcombo({
                    header: '관리자 지정',
                    dataIndex: 'col16',
                    align: 'center',
                    width: 80,
                    comboKey: 'col16',
                    mapKey: 'col16',
                    disabled: disabled,
                    renderer: formatter.yes_no
                });
            },
            col17: function (use_disable) {
                var disabled = app.roleSettings.col17_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fcombo({
                    header: '수수료 동의',
                    dataIndex: 'col17',
                    align: 'center',
                    width: 80,
                    comboKey: 'col17',
                    mapKey: 'col17',
                    disabled: disabled,
                    renderer: formatter.yes_no
                });
            },
            col18: function (use_disable) {
                var disabled = app.roleSettings.col18_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fbasic({
                    header: '원격일정',
                    dataIndex: 'col18',
                    align: 'center',
                    width: 96,
                    text_editable: true,
                    nullable: true,
                    disabled: disabled
                });
            },
            col19: function (use_disable) {
                var disabled = app.roleSettings.col18_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fbasic({
                    header: 'ID',
                    dataIndex: 'col19',
                    align: 'left',
                    width: 56,
                    disabled: disabled,
                    text_editable: true,
                    nullable: true
                });
            },
            col20: function (use_disable) {
                var disabled = app.roleSettings.col18_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fbasic({
                    header: '카드일련번호',
                    dataIndex: 'col20',
                    align: 'left',
                    width: 96,
                    disabled: disabled,
                    text_editable: true,
                    nullable: true
                });
            },
            col21: function (use_disable) {
                var disabled = app.roleSettings.col18_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fbasic({
                    header: '배송일',
                    dataIndex: 'col21',
                    align: 'left',
                    width: 80,
                    disabled: disabled,
                    text_editable: true,
                    nullable: true
                });
            },
            col24: function (use_disable) {
                var disabled = app.roleSettings.col18_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fcombo({
                    header: '증거금률 확인',
                    dataIndex: 'col24',
                    align: 'center',
                    width: 96,
                    comboKey: 'col24',
                    mapKey: 'col24',
                    disabled: disabled,
                    renderer: formatter.yes_no
                });
            },
            col43: function (use_disable) {
                var disabled = app.roleSettings.col18_disabled();
                if (use_disable) {
                    disabled = use_disable;
                }
                return fcombo({
                    header: 'ETF 신청',
                    dataIndex: 'col43',
                    align: 'center',
                    width: 96,
                    comboKey: 'col43',
                    mapKey: 'col43',
                    disabled: disabled,
                    renderer: formatter.yes_no
                });
            },
            col25: function () {
                return fcombo({
                    header: '종류',
                    dataIndex: 'col25',
                    align: 'center',
                    width: 96,
                    comboKey: 'col25',
                    mapKey: 'col25',
                    renderer: function (val) {
                        return val;
                    }
                });
            },
            col25_type2: function () {
                return fcombo({
                    header: 'AWS종류',
                    dataIndex: 'col25',
                    align: 'center',
                    width: 96,
                    comboKey: 'col25',
                    mapKey: 'col25',
                    disabled: app.roleSettings.is_not_basecamp(),
                    renderer: function (val) {
                        return val;
                    }
                });
            },
            textarea01: function (use_disable) {
                var disabled = false;
                if (use_disable) {
                    disabled = use_disable;
                }
                return fbasic({
                    header: '요청사항 및 메모',
                    dataIndex: 'textarea01',
                    align: 'left',
                    width: 320,
                    text_editable: true,
                    nullable: true,
                    disabled: disabled
                });
            },
            textarea04: function () {
                return fbasic({
                    header: '정산 메모',
                    dataIndex: 'textarea04',
                    align: 'left',
                    width: 320,
                    text_editable: true,
                    nullable: true,
                    disabled: app.roleSettings.is_not_basecamp()
                });
            },
            col26: function () {
                return fbasic({
                    header: '시작일',
                    dataIndex: 'col26',
                    align: 'left',
                    width: 96,
                    text_editable: true,
                    nullable: true,
                    disabled: app.roleSettings.is_not_basecamp()
                });
            },
            col27: function () {
                return fbasic({
                    header: '마감일',
                    dataIndex: 'col27',
                    align: 'left',
                    width: 96,
                    text_editable: true,
                    nullable: true
                });
            },
            col28: function () {
                return fbasic({
                    header: '결제일',
                    dataIndex: 'col28',
                    align: 'left',
                    width: 56,
                    text_editable: true,
                    nullable: true
                });
            },
            col29: function () {
                return fbasic({
                    header: '결제금액(만원)',
                    dataIndex: 'col29',
                    align: 'right',
                    width: 84,
                    text_editable: true,
                    nullable: true
                });
            },
            col30: function () {
                return fbasic({
                    header: '카드사',
                    dataIndex: 'col30',
                    align: 'left',
                    width: 56,
                    text_editable: true,
                    nullable: true
                });
            },
            col31: function () {
                return fbasic({
                    header: '카드번호',
                    dataIndex: 'col31',
                    align: 'left',
                    width: 112,
                    text_editable: true,
                    nullable: true
                });
            },
            col32: function () {
                return fbasic({
                    header: '유효기간',
                    dataIndex: 'col32',
                    align: 'left',
                    width: 72,
                    text_editable: true,
                    nullable: true
                });
            },
            col38: function () {
                return fbasic({
                    header: '주문번호',
                    dataIndex: 'col38',
                    align: 'left',
                    width: 180,
                    text_editable: true,
                    nullable: true
                });
            },
            col39: function () {
                return fcombo({
                    header: '계약기간',
                    dataIndex: 'col39',
                    align: 'center',
                    width: 80,
                    comboKey: 'col39',
                    mapKey: 'col39',
                    renderer: function (val) {
                        return val;
                    }
                });
            },
            col40: function () {
                return fbasic({
                    header: '원격비용(만원)',
                    dataIndex: 'col40',
                    align: 'right',
                    width: 120,
                    number_editable: true,
                    nullable: true,
                    summaryType: 'sum',
                    disabled: app.roleSettings.is_not_basecamp(),
                    renderer: formatter.commify2
                });
            },
            col42: function () {
                return fbasic({
                    header: '정산일',
                    dataIndex: 'col42',
                    align: 'center',
                    width: 60,
                    text_editable: true,
                    nullable: true
                });
            },
            col33: function () {
                return fbasic({
                    header: '클라우드비용(만원)',
                    dataIndex: 'col33',
                    align: 'right',
                    width: 120,
                    number_editable: true,
                    nullable: true,
                    disabled: app.roleSettings.is_not_basecamp(),
                    summaryType: 'sum'
                });
            },
            col34: function () {
                return fcombo({
                    header: '정기결제',
                    dataIndex: 'col34',
                    align: 'center',
                    width: 82,
                    comboKey: 'col34',
                    mapKey: 'col34',
                    renderer: formatter.yes_no
                });
            },
            col35: function () {
                return fcombo({
                    header: '포트',
                    dataIndex: 'col35',
                    align: 'left',
                    width: 88,
                    comboKey: 'products',
                    mapKey: 'product_map'
                });
            },
            col36: function () {
                return fbasic({
                    header: '시작원금(만원)',
                    dataIndex: 'col36',
                    align: 'right',
                    width: 88,
                    text_editable: true,
                    nullable: true
                });
            },
            col37: function () {
                return fbasic({
                    header: '연동비',
                    dataIndex: 'col37',
                    align: 'right',
                    width: 56,
                    text_editable: true,
                    nullable: true
                });
            }
        };
    };

    app.common.field_set_summary = field_set_summary;
}());