/*global require, module, console, app, Ext, $, _, moment*/
(function () {
    'use strict';
    var fset = app.common.field_set;

    var reloadInfo = function(){
        var order_store = Ext.getCmp('order').getStore();
        var endOrder_store = Ext.getCmp('end_order').getStore();
        var cancelOrder_store = Ext.getCmp('cancel_order').getStore();

        endOrder_store.reload();
        order_store.reload();
        cancelOrder_store.reload();
    }
    
    var editFn = function (ignore,context) { 
        var data = context.record.getData();
        delete data.rownum;

        var temp = {};
        
        _.each(_.keys(context.record.modified), function (key) {
            temp[key] = context.record.data[key];
        });

        Ext.Msg.confirm('수정 전 확인', `해당 주문정보를 ${context.value}로 수정하시겠습니까?`, function (res) {
            if (res === 'yes') {
                    if(temp){
                        var token = window.sessionStorage.getItem('login_info');
                        if(token){
                            $.ajax({
                                type: 'PUT',
                                url: '/api/cms/orderupdate/',
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("Authorization","Bearer " + token);
                                },
                                data: {
                                    substitute : JSON.stringify(temp),
                                    ordernum : data.ordernum
                                },
                                success: function () {
                                    app.combo.sendOrderPush(data, context.value);
                                    app.combo.sendOrderTele(data, context.value);
                                    context.record.commit();
                                    reloadInfo();
                                    Ext.Msg.alert('성공', "주문정보 업데이트를 완료했습니다.");
                                },
                                error: function (err) {
                                    console.log(err);
                                }
                            });
                        }else{
                            Ext.Msg.alert('권한', '권한이 없습니다.');
                        }
                    }else{
                        Ext.Msg.alert('실패', "주문정보 업데이트할 수 없습니다");
                    }
            }else{
                context.record.store.reload()
            }
        });
    };

    var deleteUser = function () {
        return {
            xtype: 'button',
            text: '주문삭제',
            disabled: app.roleSettings.deleteUser(),
            handler: function () {
                var that = this;
                deleteAction(that);
            }
        };
    };

    var deleteAction = function (that) {
        var grid = that.up().up();
        var selections = grid.getView().getSelectionModel().getSelection();
        
        if (selections.length === 0) {
            Ext.Msg.alert('안내', '선택된 주문이 없습니다.');
            return false;
        }
        Ext.Msg.confirm('삭제 전 확인', '삭제 후 데이터 복구가 불가능합니다. 진행할까요?', function (res) {
            if (res === 'yes') {
                var seqs = [];
                _.each(selections, function (selection) {
                    seqs.push(selection.data.ordernum);
                });
                var token = window.sessionStorage.getItem('login_info');
                if(token){
                    $.ajax({
                        method: 'DELETE',
                        url: '/api/cms/orderdelete/'+seqs,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Authorization","Bearer " + token);
                        },
                        success: function () {
                            grid.getStore().remove(selections);
                            reloadInfo();
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }else{
                    Ext.Msg.alert('권한', '권한이 없습니다.');
                }
            
            }
        });
    };

    app.preset.cancel_order = function(){
        return {
            uid: 'cancel_order',
            url: '/api/cms/cancel/',
            is_sub_window: true,
            maximized: true,
            use_checkbox: false,
            use_search: true,
            use_paging : true,
            fields: [
                'rownum',
                'createdAt', //계약일자
                'customer_name', // 고객명
                'customer_telephone', // 고객전화번호
                'user_name', // 회원명
                'user_phone', // 회원전화번호
                'status', //작업상황,
                'place', //시공장소
                'ordernum', //주문고유번호
                'order_id' //회원고유번호
            ],
    
            cols: [
                fset.rownum,
                fset.orderCreateDate, //계약일자
                fset.customerName, // 고객명
                fset.customerPhone, // 고객전화번호
                fset.userName, //회원명
                fset.userPhone, //회원전화번호
                fset.status, //작업상황
                fset.place, //시공장소
                fset.orderNum, //주문고유번호
                fset.orderId //회원고유번호
            ],
            features: [],
            use_filter: true,
            additional_tbars:[deleteUser()],
            listeners:{
                edit : function(ignore, context){
                    editFn(ignore,context);
                }
            }
        }
      
    }
           

  
}());